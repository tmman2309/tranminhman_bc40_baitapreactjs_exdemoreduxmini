import { MODAL, QUAN_LY_SO_LUONG, THEM_SAN_PHAM, XOA_SAN_PHAM } from "../constants/shoeConstants";

export const addToCartAction = (shoe) => ({
  type: THEM_SAN_PHAM,
  payload: shoe,
});

export const modalAction = (id) => ({
  type: MODAL,
  payload: id,
});

export const deleteItemAction = (shoe) => ({
  type: XOA_SAN_PHAM,
  payload: shoe,
});

export const manageQuantityAction = (shoe,number) => ({
  type: QUAN_LY_SO_LUONG,
  payload: {shoe,number},
});
