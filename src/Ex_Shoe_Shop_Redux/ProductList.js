import React, { Component } from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";

class ProductList extends Component {
  render() {
    return (
      <div className="row">
        {this.props.productData.map((product) => {
          return <ProductItem key={product.id} item={product} />;
        })}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    productData: state.shoeReducer.productData,
  };
};

export default connect(mapStateToProps)(ProductList);
