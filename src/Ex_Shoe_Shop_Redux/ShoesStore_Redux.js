import React, { Component } from "react";
import Cart from "./Cart";
import Modal from "./Modal";
import ProductList from "./ProductList";

export default class ShoesStore_Redux extends Component {
  // Modal
  handleModal = (id) => {
    let index = this.state.productData.findIndex((item) => {
      return item.id == id;
    });
    let cloneProductDetail = this.state.productData[index];

    this.setState({
      productDetail: cloneProductDetail,
    });
  };

  render() {
    return (
      <div>
        <Cart />
        <ProductList />
        <Modal />
      </div>
    );
  }
}
