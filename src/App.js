import './App.css';
import ShoesStore_Redux from './Ex_Shoe_Shop_Redux/ShoesStore_Redux';

function App() {
  return (
    <div className="container py-4">
      <h2 className="pb-4 text-center">Shoes Shop Redux</h2>
      <ShoesStore_Redux />
    </div>
  );
}

export default App;
